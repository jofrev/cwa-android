### 1.9.1.6

* Fix microG version check on /e/ ROMs (/e/ has two possible microG versions, one with ENF and one without. The one without ENF will now be correctly ignored.)

### 1.9.1.5

* Ignore disabled microG versions in the version check
* Set the mininum microG version to 0.2.15 (0.2.16 has additional fixes but it not strictly required)

### 1.9.1.4

* New microg version with some stability fixes
* Check for latest microG version and prompt to update it
* Link to our own FAQ from the App

### 1.9.1.3

* fix crash when opening microG ENF settings
* fix crash during exposure check
* some translation fixes (added polish translation of our additions)

### 1.9.1.2

* Lots of upstream changes, most importantly switching to Exposure Window mode (ENF v2)
* No more gray cards and minimum tracing time. The app will report a risk right away now
* Make the microG UI for exposures available (through the ENF version number in App Information)
* Allow exporting the exposure db for analysis in another app
* Fix for exposure checks sometimes taking very long
* Directly ask for battery optimization exception instead of just sending to settings
* Android 5 support

### 1.7.1.2

* Rebuild for F-Droid reproducible builds

### 1.7.1.1
Initial release of Corona Contact Tracing Germany
