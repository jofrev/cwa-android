package de.rki.coronawarnapp.ui.calendar

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import android.util.TypedValue
import android.view.LayoutInflater
import android.widget.LinearLayout
import android.widget.TextView
import de.rki.coronawarnapp.R

/**
 * Week day custom view
 */
class CalendarWeekDayView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {

    private val textView: TextView

    /**
     * Initialize the view
     *
     * Get TextView for day setup
     * SetUp layout params
     */
    init {
        LayoutInflater.from(context)
            .inflate(R.layout.fragment_calendar_day, this, true)
        textView = findViewById(R.id.dayText)

        layoutParams = LayoutParams(
            0,
            LayoutParams.WRAP_CONTENT,
            1.0f
        )
    }

    /**
     * SetUp the view from CalendarFragment
     */
    fun setUp(text: String, isSelected: Boolean = false) {
        textView.text = text

        if (isSelected) {
            // Original: textView.setTextAppearance(R.style.calendarWeekDaySelected)
            textView.apply {
                /*
                Applies the following style manually:
                    <style name="calendarWeekDaySelected">
                        <item name="android:textColor">@color/colorCalendarTodayText</item>
                        <item name="android:textSize">@dimen/font_small</item>
                        <item name="android:fontFamily">sans-serif-black</item>
                        <item name="android:letterSpacing">2</item>
                    </style>
                 */

                setTextColor(context.resources.getColor(R.color.colorCalendarTodayText))
                setTextSize(TypedValue.COMPLEX_UNIT_PX, context.resources.getDimension(R.dimen.font_small))
                typeface = Typeface.SANS_SERIF
                letterSpacing = 2F
            }
        } else {
            // Original: textView.setTextAppearance(R.style.calendarWeekDayNormal)
            textView.apply {
                /*
                Applies the following style manually:
                    <style name="calendarWeekDayNormal">
                        <item name="android:textColor">@color/colorTextPrimary2</item>
                        <item name="android:textSize">@dimen/font_small</item>
                        <item name="android:fontFamily">sans-serif</item>
                        <item name="android:letterSpacing">0.4</item>
                    </style>
                 */
                setTextColor(context.resources.getColor(R.color.colorTextPrimary2))
                setTextSize(TypedValue.COMPLEX_UNIT_PX, context.resources.getDimension(R.dimen.font_small))
                typeface = Typeface.SANS_SERIF
                letterSpacing = 0.4F
            }
        }
    }
}
