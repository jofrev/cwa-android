package de.rki.coronawarnapp.ui.launcher

import android.app.Application
import com.squareup.inject.assisted.AssistedInject
import de.rki.coronawarnapp.storage.LocalData
import de.rki.coronawarnapp.update.MicroGChecker
import de.rki.coronawarnapp.update.UpdateChecker
import de.rki.coronawarnapp.util.coroutine.DispatcherProvider
import de.rki.coronawarnapp.util.ui.SingleLiveEvent
import de.rki.coronawarnapp.util.viewmodel.CWAViewModel
import de.rki.coronawarnapp.util.viewmodel.SimpleCWAViewModelFactory

class LauncherActivityViewModel @AssistedInject constructor(
    private val updateChecker: UpdateChecker,
    dispatcherProvider: DispatcherProvider,
    val application: Application
) : CWAViewModel(dispatcherProvider = dispatcherProvider) {

    val events = SingleLiveEvent<LauncherEvent>()
    private val microGChecker = MicroGChecker(application.applicationContext)

    init {
        launch {
            val updateResult = updateChecker.checkForUpdate()
            val microGResult = microGChecker.checkForUpdate()
            when {
                updateResult.isUpdateNeeded -> LauncherEvent.ShowUpdateDialog(updateResult.updateIntent?.invoke()!!)
                microGResult.isUpdateNeeded -> LauncherEvent.ShowMicroGUpdateDialog(microGResult.updateIntent?.invoke()!!)
                LocalData.isOnboarded() -> LauncherEvent.GoToMainActivity
                else -> LauncherEvent.GoToOnboarding
            }.let { events.postValue(it) }
        }
    }

    @AssistedInject.Factory
    interface Factory : SimpleCWAViewModelFactory<LauncherActivityViewModel>
}
