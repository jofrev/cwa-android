package de.rki.coronawarnapp.update

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.util.Log
import dagger.Reusable
import de.rki.coronawarnapp.BuildConfig
import de.rki.coronawarnapp.appconfig.internal.ApplicationConfigurationCorruptException
import timber.log.Timber

@Reusable
class MicroGChecker(val context: Context) {
    fun checkForUpdate(): Result = try {
        if (isUpdateNeeded()) {
            Result(isUpdateNeeded = true, updateIntent = createUpdateAction())
        } else {
            Result(isUpdateNeeded = false)
        }
    } catch (exception: ApplicationConfigurationCorruptException) {
        Timber.e(
            "ApplicationConfigurationCorruptException caught:%s",
            exception.localizedMessage
        )

        Result(isUpdateNeeded = true, updateIntent = createUpdateAction())
    } catch (exception: Exception) {
        Timber.tag(TAG).e("Exception caught:%s", exception.localizedMessage)
        Result(isUpdateNeeded = false)
    }

    private fun isUpdateNeeded(): Boolean {
        val hasMicroGPackage = context.isPackageInstalledAndEnabled(MICROG_PACKAGENAME)
        if (hasMicroGPackage) {
            val packageInfo = context.packageManager.getPackageInfo(MICROG_PACKAGENAME, PackageManager.GET_PERMISSIONS)

            val currentVersion = packageInfo.versionCode
            var isFullMicroG = false
            var hasENF = false
            packageInfo.permissions.forEach {
                if (it.name == MICROG_PERMISSION) isFullMicroG = true
                if (it.name == ENF_PERMISSION) hasENF = true
            }

            if (isFullMicroG && hasENF) {
                val needsImmediateUpdate = VersionComparator.isVersionOlder(
                    currentVersion.toLong(),
                    BuildConfig.MINIMUM_MICROG_VERSION
                )
                Log.d(TAG, "Current microG version:$currentVersion")
                Log.d(TAG, "Required microG version:${BuildConfig.MINIMUM_MICROG_VERSION}")
                Timber.tag(TAG).e("needs update:$needsImmediateUpdate")
                return needsImmediateUpdate
            } else {
                return false
            }
        } else {
            return false
        }
    }

    private fun createUpdateAction(): () -> Intent = {
        val uriStringInPlayStore = STORE_PREFIX
        Intent(Intent.ACTION_VIEW).apply {
            data = Uri.parse(uriStringInPlayStore)
        }
    }

    data class Result(
        val isUpdateNeeded: Boolean,
        val updateIntent: (() -> Intent)? = null
    )

    companion object {
        private const val TAG: String = "MicroGChecker"
        private const val MICROG_PACKAGENAME = "com.google.android.gms"
        private const val MICROG_PERMISSION = "org.microg.gms.EXTENDED_ACCESS"
        private const val ENF_PERMISSION = "com.google.android.gms.nearby.exposurenotification.EXPOSURE_CALLBACK"

        private const val STORE_PREFIX = "https://microg.org/download.html"
    }
}

fun Context.isPackageInstalledAndEnabled(packageName: String): Boolean {
    try {
        packageManager.getPackageInfo(packageName, 0)
        return (packageManager.getApplicationEnabledSetting(packageName) == PackageManager.COMPONENT_ENABLED_STATE_DEFAULT
            || packageManager.getApplicationEnabledSetting(packageName) == PackageManager.COMPONENT_ENABLED_STATE_ENABLED)
    } catch (e: PackageManager.NameNotFoundException) {
        return false
    }
}
