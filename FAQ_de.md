# Häufig gestellte Frage (FAQ)

### Wo finde ich die Exposure Notification Einstellungen von microG in der CCTG-App?

Öffnen Sie **App-Informationen** im Overflow-Menü und klicken Sie auf das Feld `ENF Version XYZ`, daraufhin werden Sie automatisch zu den microG Exposure Notification Framework (ENF) Einstellungen weitergeleitet.  
Dabei wird berücksichtigt, ob die App ein systemweit installiertes oder das integrierte microG verwendet.

### Warum zeigen mir microG/CWA Companion Risikobegegnungen an, die CCTG-App aber nicht?

Ab Version 1.9.1 benutzt die App Version 2 des ENF, mit der Änderungen in der Bewertung bzw. Erfassung von Risikobegegnungen eingeführt wurden (s. der [offizielle Blogpost](https://www.coronawarn.app/en/blog/2020-12-17-risk-calculation-exposure-notification-framework-2-0/)):

> Vereinfacht ausgedrückt: Unter dem Exposure Notification Framework in Version 2 werden vom Betriebssystem Begegnungen erfasst, die ein geringeres Risiko als „niedriges Risiko“ (grün) aufweisen. Diese sind aus aktueller epidemiologischer Sicht nicht relevant und werden von der Corona-Warn-App (CWA) herausgefiltert.

### Warum kann die App im Hintergrund laufen? Ich habe gehört, dass das eigentlich gar nicht geht.

Die App wurde auf verschiedenen Android ROMs getestet, mit dem Ergebnis, dass sie problemlos im Hintergrundbetrieb laufen kann und zwar unabhängig davon, ob das integrierte oder systemweit installierte microG verwendet wird.

Nichtsdestoweniger gibt es einige Dinge, die Sie überprüfen sollten (spätestens, wenn Sie das Gefühl haben, dass die App nicht korrekt funktioniert):

* Einige Hersteller von Android-Smartphones sind dafür bekannt besonders aggressiv gegen Anwendungen, die im Hintergrund laufen, vorzugehen. Unter folgendem Link können Sie überprüfen, ob dies für Ihr Smartphone und ROM der Fall ist: https://dontkillmyapp.com/.  
Bei einigen Herstellern haben Sie als Benutzer die Möglichkeit etwas dagegen zu unternehmen.
* Aktivieren Sie in den App-Einstellungen die Funktion **Priorisierte Hintergrundaktivität**.
* Verzichten Sie wenn möglich darauf den Energiesparmodus Ihres Smartphones zu aktivieren, da dieser einen negativen Einfluss auf die Hintergrundaktualisierung der App haben kann.

### Brauche ich microG oder Signature Spoofing um die App zu benutzen?

Nein, microG ist direkt in die App integriert, dadurch funktioniert sie auch auf Smartphones, auf denen microG nicht installiert ist. Wenn Sie microG auf Ihrem Smartphone bereits installiert haben, benutzt die App automatisch diese systemweite Installation anstatt ihrer eigenen integrierten.

### Warum benötigt die App Zugriff auf meinen Standort?

Die App greift weder auf Ihren GPS- noch Netzwerk-Standort zu.  
Für Android ist Bluetooth-Scanning allerdings eine Art von Standortzugriff, weil es theoretisch möglich ist, aus den gewonnenen Informationen Ihren ungefähren Standort zu ermitteln (s. https://stackoverflow.com/a/44291991/1634837).  
CCTG selbst versucht in keiner Form Ihren Standort zu bestimmen oder gar zu verfolgen.

In Android 11 erlaubt Google seiner Play-Services-Implementation des ENF Bluetooth-Scanning im Hintergrund auszuführen, [ohne spezielle Erlaubnis für die Standortermittlung](https://android.googlesource.com/platform/packages/apps/Bluetooth/+/refs/tags/android-11.0.0_r16/res/values/config.xml#118) beim Benutzer anzufragen. CCTG ist das natürlich nicht erlaubt, weswegen die App auch in Android 11 weiterhin Ihre Erlaubnis für die Standortermittlung benötigt.

### Wie oft veröffentlicht ihr neue Updates? Wann werden sie bei F-Droid bereit gestellt?

Kurze Zeit nachdem ein Update von der Corona-Warn-App veröffentlich wird, veröffentlichen wir eine neue Version von CCTG. Diese neue Version ist sofort in unserem Repository verfügbar.

In der ersten Stufe unseres transparenten Staged Rollouts wird die App noch nicht als *vorgeschlagen* markiert. Dadurch empfiehlt Ihr F-Droid-Client sie Ihnen noch nicht als Update. Sie können sie natürlich trotzdem installieren, wenn Sie möchten.  
Wir tun dies, um von Benutzern, die sich aktiv dafür entscheiden eine möglicherweise noch nicht stabile Version der App auszuprobieren, Feedback über Abstürze und sonstige Probleme der App zu sammeln.

Sobald wir der Meinung sind, dass alle Probleme behoben wurden, markieren wir die neueste Version als *vorgeschlagen*, wodurch alle Benutzer unseres Repositorys das Update sofort erhalten, wenn ihr F-Droid-Client es das nächste Mal aktualisiert.

Einige Zeit später (wie üblich für F-Droid) wird das Update dann auch im offiziellen F-Droid Repository verfügbar sein.  
Weil unsere App reproduzierbare Builds unterstützt, ist die über das offizielle F-Droid-Repository verfügbare APK exakt die gleiche wie die aus unserem Repository.

Folgen Sie unserem [Mastodon-Account](https://social.tchncs.de/@CCTG), um über neue Versionen der App informiert zu werden.

### Was genau unterscheidet CCTG von CWA?

Unabhänging davon ob microG oder Google Play Services installiert sind, verwendet die offizielle Corona-Warn-App proprietäre Software um mit der Exposure Notification API zu interagieren.

Corona Contact Tracing Germany (CCTG) ersetzt diesen proprietären Teil mit einer anderen Bibliothek vom [microG-Projekt](https://microg.org). Das bedeutet, dass CCTG (im Gegensatz zu CWA) in Gänze freie Software ist.

Darüberhinaus enthält unsere App alle benötigten Komponenten um auch eigenständig zu funktionieren, wenn microG *nicht* auf Ihrem Smartphone installiert ist.

Abgesehen davon liegt der Fokus dieses Projekts darauf Änderungen im Branding der App (Titel, Logo, Datenschutzrichtlinien, AGB, Impressum...) auf einem Stand mit der originalen Version zu halten.

### Ich habe vorher CWA benutzt, möchte jetzt aber zu CCTG wechseln. Was muss ich tun?

Falls Sie CWA mit Googles ENF-Implementation benutzt haben sollten, müssen Sie beide Apps zwei Wochen lang parallel verwenden, bevor Sie CWA deinstallieren können. Nach diesen zwei Wochen sind alle alten Exposure-Daten gelöscht und CCTG ist auf ein und demselben Stand mit CWA.  
Wenn Sie in ein positives Testergebnis erhalten, bevor die zwei Wochen vergangen sind, sollten Sie es in CWA melden, um sicherzustellen, dass alle ihre Kontakte korrekt benachrichtigt werden.  
Nach den zwei Wochen können Sie CWA bedenkenlos deinstallieren.  
Uns sind keine Probleme bekannt, wenn beide Apps parallel verwendet werden.

Sollten Sie CWA auf einem Smartphone mit microG verwendet haben, ist die Migration zu CCTG denkbar einfach: Sie können CWA einfach deinstallieren und CCTG installieren, ohne dass Exposure-Daten verloren gehen.  
Die App wird Ihnen in diesem Fall zwar anzeigen, dass sie erst seit 0 Tagen benutzt wird, was aber ein rein kosmetischer "Fehler" ist und keinen Einfluss auf die Exposure Notifications bzw. das Melden von Risikobegegnungen hat.

### CCTG zeigt mir an, dass ich eine aktuellere Version von microG installieren soll, das funktioniert aber nicht. Was kann ich tun?

Einige Android-ROMs haben ihre eigene Version von microG integriert, die mit einem anderen Schlüssel als dem des microG-Projekts signiert ist.  
Falls das bei Ihnen der Fall sein sollte, bleibt Ihnen nichts anderes übrig als darauf zu warten, dass ihr ROM seine integrierte microG-Version aktualisiert, bevor Sie eine neuere Version von CCTG installieren und benutzen können.

Ab Version 1.9.1.X benötigt CCTG mindestens Version 0.2.15 von microG. Die Version davor (1.7.1) benötigt mindestens 0.2.14, was aber nicht überprüft bzw. erzwungen wurde.

Falls Sie CCTG bereits aktualisiert haben, können Sie versuchen wieder eine ältere Version der App zu installieren, die zu ihrer microG Version kompatibel ist (s. [dieser Kommentar](https://codeberg.org/corona-contact-tracing-germany/cwa-android/issues/51#issuecomment-165230)).  
Sie können CCTG auch einfach deinstallieren und die ältere Version neu installieren ohne Exposure-Daten zu verlieren, da diese in Ihrer microG Installation gespeichert sind. In diesem Fall werden allerdings ausstehende und bereits empfangene Testergebnisse gelöscht. Außerdem beginnt die App erneut von Tag 0 an zu zählen, was jedoch, wie oben bereits beschrieben, keinen weiteren Einfluss auf die Funktion der App hat.
