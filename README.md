# Corona Contact Tracing Germany

## Talk to us

* Join the matrix room: [#cctg:bubu1.eu](https://matrix.to/#/#cctg:bubu1.eu), also bridge to XMPP: xmpp:cctg@conference.jabber.de?join
* Follow us on Mastodon: [@CCTG@social.tchncs.de](https://social.tchncs.de/@CCTG)

## Get The App

Our [F-Droid repository](https://bubu1.eu/cctg/fdroid/repo/?fingerprint=f3f30b6d212d84aea604c3df00e9e4d4a39194a33bf6ec58db53af0ac4b41bec) contains *beta* versions and is updated with new releases immediately. After a few days, stable releases will also be available in the standard f-droid.org repo. You can also verify the builds yourself, see [Reproducible Builds](#reproducible-builds).

<p align="center">
<a href="https://f-droid.org/packages/de.corona.tracing">
<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
    alt="Get it on F-Droid"
    width="180px">
</a>
<br>
<a href="https://liberapay.com/CCTG/">
<img src="https://liberapay.com/assets/widgets/donate.svg" alt="Donate" width="105px">
</a>
</p>

## About

This is a fork of [CWA](https://github.com/corona-warn-app/cwa-app-android/) without proprietary dependencies. While the German Corona Warn App itself is Free Software, it depends on Google's proprietary [Exposure Notification Framework](https://www.google.com/covid19/exposurenotifications/). This fork instead uses the drop-in compatible [microg implementation](https://github.com/microg/GmsCore/issues/1166).

This is currently very experimental and mostly untested. Use at your own risk! It should work on any Android 6+ device regardless of installed play-services or microg versions.

If there's microg already installed on the system it will use the exposure notification framework from there. Otherwise it will use the bundled implementation. It will never connect to the play services exposure notification framework.

## Translations

There's currently no translation system in place as most translation are taken directly from CWA. There's some small bits left to translate/fix for Romanian, Bulgarian and Turkish. Please contact us!

* Polish Translations of our text additions contributed by [GenomZ](https://github.com/GenomZ).

# FAQ

### How do I access the microG exposure notitifaction settings in the standalone app?

Go to App Information (via the three-dot/kebab menu) and then tap on the `ENF Version XYZ` field. It'll magically take to to the correct (embedded or system) microG ENF settings page.

### microG/CWA Companion shows some exposures but the app says there were none.

Starting with version 1.9.1 this is expected, see the [offical blog post](https://www.coronawarn.app/en/blog/2020-12-17-risk-calculation-exposure-notification-framework-2-0/):

> In simplified terms: Under Exposure Notification Version 2.0, the operating system also logs encounters with a risk lower than "low risk" (green). However, since these encounters are not relevant from the current epidemiological perspective, the Corona-Warn-App filters them out.

### How does the app manage to stay alive in the background? Didn't everyone say that's impossible?

The app was tested on a variety of Android ROMs and found to generally behave fine for backgound operations, with or without a system level microG version.

That said, there's some things you should check for everything to work correctly:

* Some Android vendors are known to have very aggressive background killing services, you could check if this is the case for your ROM here: https://dontkillmyapp.com/. For some vendors there's also steps you can take as a user to make it work better.
* Make sure the "Prioritized Backgroud Activity" option in the apps settings menu is enabled.
* Avoid to enable your phone's battery saving mode when possible. It's likely that this will make the scanning functionality less reliable.

### Do I need microG/signature spoofing for this?

No, this app bundles a standalone version of the microG implementation that will get used when there's no microG system installation found.

### Why does the app need location permission?

The app doesn't access GPS or Network location but Android considers bluetooth scanning a form of location access (because you could derive location information from the info you could get there), see here for details: https://stackoverflow.com/a/44291991/1634837. CCTG doesn't do any location tracking though.

On Android 11 Google allowed the play services ENF implementation to do bluetooth scanning in the background [without special location permission](https://android.googlesource.com/platform/packages/apps/Bluetooth/+/refs/tags/android-11.0.0_r16/res/values/config.xml#118). CCTG isn't whitelisted of course and thus needs to still ask for full location permission in Android 11.

### When will updates be released? When is the app coming to F-Droid?

Shortly after an update to CWA is released, we will release a new version of CCTG. New versions are immediately available on our repo.

In the first stage of our "transparent staged rollout", the app is not marked as "suggested" yet, meaning that your F-Droid client won't recommend it as an update, though you can install it manually. We do this to be informed about crashes and other issues by users who knowingly choose to try out a version that might not be stable yet.

Once we feel that all issues are sorted out, we will mark the most recent version as "suggested", causing users of our repository to receive the update immediately (once their F-Droid client refreshes the repository).

After some time, the latest version also appears in the official F-Droid repository, though as usual for F-Droid, this can take a while. F-Droid will serve exactly the APK that we also have in our repo, because our app builds reproducibly.

You might want to follow our [mastodon account](https://social.tchncs.de/@CCTG), where we will announce new versions.

### What is the difference to CWA?

The official Corona-Warn-App build contains a proprietary component to interact with the Exposure Notifications API, even if microg is installed instead of Google Play Services.

Corona Contact Tracing Germany replaces this proprietary component with a different library provided by the [microg](https://microg.org) project, meaning that it is built as fully free software (in contrast to Corona-Warn-App).

Our app also ships with the relevant components to also function as a standalone app if microg is *not* installed.

Besides that, the project's main task is to keep changes related to branding (app title, icon, privacy policy, terms of service, imprint…) in sync with new upstream versions.

### How to migrate from CWA

If you have been running CWA with google's exposure notification framework before, you'll have to use both apps in parallel for two weeks. After two weeks all past exposure data will have been deleted and all new data is also recorded by the CCTG app. If you have a positive test result, you'll have to report this through CWA until the two weeks are over. You can uninstall CWA after those two weeks. As far as we know there's no downsides to running both apps in parallel.

If you have been using CWA on a phone with microG migration is super simple instead: Just uninstall CWA and install CCTG, no exposure data will be lost in the process. The app will say that it has only been active for 0 days again, but this is purely cosmetic and does not have an effect on the exposure notification and reporting functionality.

### CCTG says I need to update microG but this isn't working

Some ROMs come with a version of microG that is signed with a different key from the one the microG project distributes, in this case you need to wait for your ROM to update their built-in microG version before you can use the new CCTG version.

CCTG version 1.9.1.X needs microG at least version 0.215 to work. The previous version (1.7.1) needed at least version 0.2.14 but this wasn't enforced.

If you already updated you can try following [this comment](https://codeberg.org/corona-contact-tracing-germany/cwa-android/issues/51#issuecomment-165230) to see how to downgrade to 1.7.1 again. You can also uninstall CCTG and reinstall the older version. Your exposure data (but not pending or received test results) will be kept intact inside the microG installation. The app will start counting from day 0 again but this makes no functional difference.


### Official Corona-Warn-App FAQ

See also here: 

* DE: https://www.bundesregierung.de/corona-warn-app-faq
* EN: https://www.bundesregierung.de/corona-warn-app-faq-englisch


## Reproducible Builds

See [docs/rebuilding.md](./docs/rebuilding.md) on how to reproduce the official builds.
